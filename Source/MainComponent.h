/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "../Source/MidiMessageComponent.hpp"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MidiInputCallback,
                        public Slider::Listener,
                        public ComboBox::Listener,
                        public TextButton::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    void handleIncomingMidiMessage (MidiInput*, const MidiMessage&) override;
    void buttonClicked (Button* button) override;
    void sliderValueChanged(Slider* slider) override;
    void comboBoxChanged(ComboBox* comboBox) override;

private:
    AudioDeviceManager audioDeviceManager;
    Label midiLabel;
    
    MidiMessageComponent midiMessageComponent;
    
    TextButton sendButton;
    
    int channelNum;
    int noteNum;
    float velocity;
    int messageID;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
