//
//  MidiMessageComponent.hpp
//  JuceBasicWindow
//
//  Created by Kyle Edwards on 11/11/2016.
//
//

#ifndef MidiMessageComponent_hpp
#define MidiMessageComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

class MidiMessageComponent   :  public Component,
                                public MidiInputCallback,
                                public Slider::Listener,
                                public ComboBox::Listener,
                                public TextButton::Listener
{
public:

    MidiMessageComponent();
    ~MidiMessageComponent();
    
    void resized() override;
    void handleIncomingMidiMessage (MidiInput*, const MidiMessage&) override;
    void buttonClicked (Button* button) override;
    void sliderValueChanged(Slider* slider) override;
    void comboBoxChanged(ComboBox* comboBox) override;
    
private:
    ComboBox messageType;
    Label messageTypeText;
    
    Slider channelIncDec;
    Label channelText;
    
    Slider numberIncDec;
    Label numberText;
    
    Slider velocityIncDec;
    Label velocityText;

};


#endif /* MidiMessageComponent_hpp */
