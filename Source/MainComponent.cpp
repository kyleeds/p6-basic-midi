/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (570, 400);
    channelNum = 1;
    
    //2 spaces between "Impulses" - Enabling the Impulse MIDI input.
    audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true);
    // adding MainComponent class to be a listener to the MIDI input.
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    
    
    
    sendButton.setButtonText("Send");
    addAndMakeVisible(sendButton);
    sendButton.addListener(this);
    
    addAndMakeVisible(midiLabel);
    
    //Setting the default midi output to SimpleSynth
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
}

MainComponent::~MainComponent()
{
    //Ensuring that MainComponent listener stops listening to the MIDI input when the program closes.
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    midiLabel.setBounds (10, 110, getWidth() - 20, 40);
    
    sendButton.setBounds(510, 30, 50, 20);
}
void MainComponent::handleIncomingMidiMessage (MidiInput*, const MidiMessage& message_)
{
    DBG ("Midi Message Received.\n");
    
    String midiText;
    if (message_.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel " << message_.getChannel();
        midiText << ":Number" << message_.getNoteNumber();
        midiText << ":Velocity" << message_.getVelocity();
    }
    else if (message_.isProgramChange())
    {
        midiText << "Program Number" << message_.getProgramChangeNumber();
    }
    else if (message_.isChannelPressure())
    {
        midiText << "Channel Pressure Value" << message_.getChannelPressureValue();
    }
    else if (message_.isPitchWheel())
    {
        midiText << "Pitch Wheel Value" << message_.getPitchWheelValue();
    }
    else if (message_.isController())
    {
        midiText << "Controller Number" << message_.getControllerNumber();
        midiText << ":Controller Value" << message_.getControllerValue();
    }
    midiLabel.getTextValue() = midiText;
    
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow (message_);
}
void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider == &channelIncDec)
    {
        channelNum = channelIncDec.getValue();
        DBG("Channel Value:" << channelIncDec.getValue() << "\n");
    }
    else if (slider == &numberIncDec)
    {
        noteNum = numberIncDec.getValue();
        DBG("Number Value:" << numberIncDec.getValue() << "\n");
    }
    else if (slider == &velocityIncDec)
    {
        velocity = velocityIncDec.getValue();
        DBG("Velocity Value:" << velocityIncDec.getValue() << "\n");
    }
}
void MainComponent::buttonClicked(Button* button)
{
    DBG("Send Button Clicked\n");
    if (messageID == 1)
    {
        MidiMessage message (MidiMessage::noteOn(channelNum, noteNum, (float)velocity/127));
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow (message);
    }
    else if (messageID == 2)
    {
        
        MidiMessage message (MidiMessage::controllerEvent(channelNum, velocity, noteNum));
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow (message);
    }
    else
    {
        MidiMessage message (MidiMessage::pitchWheel(channelNum, noteNum));
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow (message);
    }
}
void MainComponent::comboBoxChanged(ComboBox* comboBox)
{
    messageID = messageType.getSelectedId();
    DBG("ComboBox Changed\n");
}