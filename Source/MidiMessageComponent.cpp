//
//  MidiMessageComponent.cpp
//  JuceBasicWindow
//
//  Created by Kyle Edwards on 11/11/2016.
//
//

#include "MidiMessageComponent.hpp"

MidiMessageComponent::MidiMessageComponent()
{
    String messageTypeLabelText = "MessageType";
    messageTypeText.setText(messageTypeLabelText, dontSendNotification);
    addAndMakeVisible(messageTypeText);
    
    messageType.addItem("Note", 1);
    messageType.addItem("Control", 2);
    messageType.addItem("Pitch Wheel", 3);
    messageType.setSelectedId(1);
    addAndMakeVisible(messageType);
    messageType.addListener(this);
    
    String channelLabelText = "Channel";
    channelText.setText(channelLabelText, dontSendNotification);
    addAndMakeVisible(channelText);
    
    channelIncDec.setSliderStyle(Slider::IncDecButtons);
    channelIncDec.setRange(1, 3, 1);
    addAndMakeVisible(channelIncDec);
    channelIncDec.addListener(this);
    
    String numberLabelText = "Number/Value";
    numberText.setText(numberLabelText, dontSendNotification);
    addAndMakeVisible(numberText);
    
    numberIncDec.setSliderStyle(Slider::IncDecButtons);
    numberIncDec.setRange(0, 127, 1);
    addAndMakeVisible(numberIncDec);
    numberIncDec.addListener(this);
    
    String velocityLabelText = "Velocity/Ctr Num";
    velocityText.setText(velocityLabelText, dontSendNotification);
    addAndMakeVisible(velocityText);
    
    velocityIncDec.setSliderStyle(Slider::IncDecButtons);
    velocityIncDec.setRange(0, 127, 1);
    addAndMakeVisible(velocityIncDec);
    velocityIncDec.addListener(this);
}
MidiMessageComponent::~MidiMessageComponent()
{
    
}
void MidiMessageComponent::resized()
{
    messageTypeText.setBounds(10, 10, 100, 20);
    messageType.setBounds(10, 30, 100, 20);
    channelText.setBounds(120, 10, 100, 20);
    channelIncDec.setBounds(120, 30, 120, 20);
    numberText.setBounds(250, 10, 100, 20);
    numberIncDec.setBounds(250, 30, 120, 20);
    velocityText.setBounds(380, 10, 100, 20);
    velocityIncDec.setBounds(380, 30, 120, 20);
}